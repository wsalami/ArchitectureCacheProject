#include<stdio.h>

#define SIZE 30

int main() {
	int mat1[SIZE][SIZE] = {1};
	int mat2[SIZE][SIZE] = {2};
	int res[SIZE][SIZE];

	// Multiply the two matrix
	int i, j, k, sum;
	for(i=0; i<SIZE; i++) {
		for(j=0; j<SIZE; j++) {
			sum = 0;
			for(k=0; k<SIZE; k++) {
				sum = sum + mat1[i][k] * mat2[k][j];
			}
			res[i][j] = sum;
		}
	}
	return 0;
}
