/**
 * @author Loan
 * @date 11/16/2017.
 */

/**
 * A slot in the cache table
 *
 */
public class Slot {
    private static long DEFAULT_TAG = -1;
    private long tag;

    public Slot() {
        this(DEFAULT_TAG);
    }

    public Slot(long tag) {
        this.tag = tag;
    }

    public boolean isTag(long otherTag) {
        return tag == otherTag;
    }

    public boolean isFree() {
        return tag == DEFAULT_TAG;
    }

    public void setTag(long newTag) {
        tag = newTag;
    }

}
