import java.util.Objects;

/**
 * @author Loan
 * @date 11/13/2017.
 */
public class Main {
    public final static int BITS = 64;
    public final static int OFFSET_BIT_NUMBER = 6;

    public static void main(String[] args) {
        if (args.length == 3) {
            String tracePath = args[0];
            int rowsNumber = Integer.parseInt(args[1]);
            boolean preFetch = Objects.equals(args[2], "1");

            // Display information text
            System.out.print("Calculation hit ratio % for trace file "+tracePath+ " with " + rowsNumber+" row number with the prefetching ");
            if (preFetch)
                System.out.println("enabled...");
            else
                System.out.println("disabled...");

            // Calculate the hit ratio
            double hitRatio = Utils.getCacheHitRatio(rowsNumber, tracePath, preFetch);
            System.out.println("Hit ratio : "+hitRatio+"%");
        }
        else {
            System.out.println("The arguments number is wrong.");
            System.out.println("<path to trace file> <number of row in cache> <flag for prefteching>");
            System.out.println("e.g :");
            System.out.println("trace.txt 16 1");
            System.out.println("traces/ls_pinatrace.txt 32 1");
            System.out.println("traces/C_pinatrace.txt 4 0");
        }
    }
}
