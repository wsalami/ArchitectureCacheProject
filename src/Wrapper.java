/**
 * @author Loan
 * @date 11/21/2017.
 */

import java.math.BigInteger;

/**
 * Wrapper of a big integer and a read or write in the same object
 */
public class Wrapper {
    public BigInteger bigInteger;
    public boolean isRead; // if not it is Write

    public Wrapper(BigInteger bigInteger, boolean isRead) {
        this.bigInteger = bigInteger;
        this.isRead = isRead;
    }
}
