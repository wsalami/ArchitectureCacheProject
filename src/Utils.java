import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @author Loan
 * @date 11/21/2017.
 */
public class Utils {

    public  static Wrapper[] parsePinTraceFiles(String filePath) {
        ArrayList<Wrapper> retList = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            for(String line; (line = br.readLine()) != null; ) {
                String[] split = line.split(" ");
                if(split.length == 3) {
                    boolean isRead = split[1].equals("R");

                    String bigIntegerStr = split[2].substring(2);
                    BigInteger bigInteger = new BigInteger(bigIntegerStr, 16);

                    retList.add(new Wrapper(bigInteger, isRead));
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return retList.toArray(new Wrapper[retList.size()]);
    }

    /**
     * Analyse a given trace file path for increasing value of raw number for after make graphs
     * We then copy paste the result to Excel to create the graphs and tables
     *
     * @param tracePath
     */
    public static void totalAnalyse(String tracePath) {
        String line1 = "Hit%\t", line2 = "With Prefetch\t", line3 = "Without Prefetch\t";
        for (int rowsNumbers =1; rowsNumbers<=65536; rowsNumbers*=2) {
            line1 += rowsNumbers+"\t";
            line2 += getCacheHitRatio(rowsNumbers, tracePath, true)+"%\t";
            line3 += getCacheHitRatio(rowsNumbers, tracePath, false)+"%\t";
        }
        System.out.println(line1);
        System.out.println(line2);
        System.out.println(line3);
    }

    /**
     * Create the cache sending him addresses one by one  then return the final hit ratio
     *
     * @param rowsNumbers The number of rows that the user want that will be created in the cache
     * @param tracePath the path to the trace file
     * @param prefetch if prefetching is enabled or not
     * @return the hit ratio of the created cache table with the previous parameters
     */
    public static double getCacheHitRatio(int rowsNumbers, String tracePath, boolean prefetch) {
        int rawsBitNumber = (int) Math.floor(Math.log(rowsNumbers)/Math.log(2));
        CacheTable cache = new CacheTable(rowsNumbers);

        Wrapper[] allAdress = parsePinTraceFiles(tracePath);
        for (int i = 0; i < allAdress.length; i++) {
            System.out.print(i*100/allAdress.length+"%\r"); // loading %
            Address address = new Address(allAdress[i].bigInteger, Main.OFFSET_BIT_NUMBER, rawsBitNumber);
            cache.getDataAtAdress(address, allAdress[i].isRead);
            if (prefetch && allAdress[i].isRead) {
                address.incrementRawIndex(rowsNumbers);
                cache.addAdress(address);
            }
        }
        return cache.getHitPercentage();
    }
}
