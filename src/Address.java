import java.math.BigInteger;

/**
 * @author Loan
 * @date 11/16/2017.
 */

/**
 * Memory virtual address as presented in the assignment
 */
public class Address {
    private final BigInteger tag;
    private int rowIndex;

    public Address(BigInteger address, int offsetBitNumber, int rowBitNumber) {
        // The tag is the x most significant bit value, where x is 64-(<raw bit number>+<offset bit number>)
        // Here we shift this bits in order to have it
        tag = address.shiftRight(rowBitNumber+offsetBitNumber);
        // The raw index is n bits long after the offset
        rowIndex = address.mod(tag).shiftRight(offsetBitNumber).intValue();
    }

    public long getTag() {
        return tag.longValue();
    }

    public int getRowIndex() {
        return rowIndex;
    }

    /**
     * This function is called by the prefetcher in order to increment the row index of the created "fake" address
     *
     * @param maxRawIndex the maximum rax index (we will go back to 0 if it is exceeded
     */
    public void incrementRawIndex(int maxRawIndex) {
        rowIndex = (rowIndex +1)%maxRawIndex;}
}
