/**
 * @author Loan
 * @date 11/16/2017.
 */

/**
 * This represent the cache table, holding each data slot
 */
public class CacheTable {
    private final static int COLS_NUMBER = 4; // 4-way associativity
    private final int rowsNumber;
    private final Slot[][] memory;

    private long hitCounter;
    private long missCounter;

    public CacheTable(int rowsNumber) {
        this.rowsNumber = rowsNumber;

        this.memory = new Slot[rowsNumber][COLS_NUMBER];
        for (int i = 0; i < rowsNumber; i++) {
            for (int j = 0; j < COLS_NUMBER; j++) {
                memory[i][j] = new Slot();
            }
        }

        this.hitCounter = 0;
        this.missCounter = 0;
    }

    /**
     * We simulate the access of data (which is the goal of a cache in normal circumstances)
     * As we don't care about the content it will just update counter depending if their is a Hit or a Miss
     * It will update the cache table if a Miss occur
     *
     * @param address The address to access the value
     */
    public void getDataAtAdress(Address address, boolean isReadAcess) {
        if (isAddressInCache(address)) {
            if (isReadAcess)
                hitCounter++;
        }
        else {
            if (isReadAcess)
                missCounter++;
            addAdress(address);
        }
    }

    /**
     * In case of a Miss we update the corresponding slot in the cache table by adding the address
     * It can also be call externally for prefetching next cache block
     *
     * @param address The address to add to the cache table
     */
    public void addAdress(Address address) {
        if (!isAddressInCache(address)) {
            int rowIndex = address.getRowIndex();
            // Look if one is free
            for (int i = 0; i < COLS_NUMBER; i++) {
                Slot curSlot = memory[rowIndex][i];
                if (curSlot.isFree()) {
                    updateSlot(curSlot, address);
                    return;
                }
            }

            // If none of the slots are free we apply the replacement policy
            removePolicyAcessed(address);
        }
    }

    /**
     * A remove policy, correspond to the Accessed remove policy described in the assignment.
     * More information on the report
     *
     * @param address
     */
    void removePolicyAcessed(Address address) {
        int rowIndex = address.getRowIndex();
        // We update the more to the left slot
        Slot mostLeftSlot = memory[rowIndex][0];
        updateSlot(mostLeftSlot, address);
    }


    /**
     * The remove policy, here we used a LRC, Least Recently Created approach
     * It is no longer used nor needed as argued in the report.
     *
     * @param address
     **/
    /*
    @Deprecated
    private void removePolicyLRC(Address address) {
        // We first find the block that have been created the last
        int rawIndex = address.getRowIndex();
        int indexLowestCounter = -1;
        long currentLowestCounter = Long.MAX_VALUE;
        for (int i = 0; i < COLS_NUMBER; i++) {
            Slot curSlot = memory[rawIndex][i];
            if(!curSlot.isFree()) {
                long slotCounter = curSlot.getCounterID();
                if (slotCounter < currentLowestCounter) {
                    currentLowestCounter = slotCounter;
                    indexLowestCounter = i;
                }
            }
        }
        // If none of the slots are free we apply the replacement policy
        // We uesd the earliest acessed slot and we update it
        Slot earliestSlot = memory[rawIndex][indexLowestCounter];
        updateSlot(earliestSlot, address);
    }
    */

    /**
     * Update a given slot with a given address.
     *
     * @param slot The slot to update
     * @param address The address to update the slot with
     */
    private void updateSlot(Slot slot, Address address) {
        slot.setTag(address.getTag());
    }

    /**
     * Check wherever a given address is already in the cache or not
     *
     * @param address The address to check
     * @return True if the Address is present
     */
    private boolean isAddressInCache(Address address) {
        int rowIndex = address.getRowIndex();
        for (int i = 0; i < COLS_NUMBER; i++) {
            if(memory[rowIndex][i].isTag(address.getTag()))
                return true;
        }
        return false;
    }

    /**
     * Give the current hit ratio percentage the cache is currently at
     *
     * @return the hit ratio as a double percentage value between 0 and 100
     */
    public double getHitPercentage(){
        long total = hitCounter+missCounter;
        return hitCounter*100/(double)total;
    }

}
